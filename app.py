import requests
import configparser
from flask import Flask, render_template
import datetime

app = Flask(__name__)


@app.route('/')
def render_results():
    city_name = get_city_name()
    api_key = get_api_key()
    data = get_weather_results(city_name, api_key)

    weather = {
        'temp': "{0:.0f}".format(data['main']['temp']),
        'location': data['name'],
        'code': data['sys']['country'],
        'wind': data['wind']['speed'],
        'feels_like': data['main']['feels_like'],
        'pressure': data['main']['pressure'],
        'humidity': data['main']['humidity'],
        'icon': data['weather'][0]['icon'],
        'description': data['weather'][0]['description'],
        'update_time': datetime.datetime.now().strftime('%H:%M:%S'),
    }

    return render_template('index.html', weather=weather)


def get_api_key():
    config = configparser.ConfigParser()
    config.read('config.ini')
    return config['openweathermap']['api']


def get_city_name():
    config = configparser.ConfigParser()
    config.read('config.ini')
    return config['openweathermap']['city']


def get_weather_results(city_name, api_key):
    api_url = "https://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid={}".format(city_name, api_key)
    r = requests.get(api_url)
    return r.json()


if __name__ == '__main__':
    app.run(host='0.0.0.0')

# app.run(host='0.0.0.0', port=8080)
