FROM alpine:latest

RUN apk add --no-cache --update python3 py3-pip bash

WORKDIR /awesome-weather-project

ADD . /awesome-weather-project/

RUN pip install -r requirements.txt

CMD ["gunicorn"  , "-b", "0.0.0.0:8000", "app:app"]

# CMD gunicorn --bind 0.0.0.0:$PORT wsgi

# CMD ["python", "app.py"]